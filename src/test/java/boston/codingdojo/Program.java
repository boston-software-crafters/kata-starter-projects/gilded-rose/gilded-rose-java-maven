package boston.codingdojo;

import java.util.Arrays;
import java.util.List;

/**
 * @author Danil Suits (danil@vast.com)
 */
class Program {
    public static void main(String[] args) {
        GildedRose app = new GildedRose(
                items(
                        item(name("+5 Dexterity Vest"), sellIn(10), quality(20)),
                        item(name("Aged Brie"), sellIn(2), quality(0)),
                        item(name("Elixir of the Mongoose"), sellIn(5), quality(7)),
                        item(name("Sulfuras, Hand of Ragnaros"), sellIn(0), quality(80)),
                        item(
                                name("Backstage passes to a TAFKAL80ETC concert"),
                                sellIn(15),
                                quality(20)
                        ),
                        item(name("Conjured Mana Cake"), sellIn(3), quality(6))
                )
        );

        app.updateQuality();
    }

    static Item[] items (Item... items) {
        return items;
    }
    
    static Item item(String name, int sellIn, int quality) {
        return new Item(name, sellIn, quality);
    }
    
    static String name(String s) {
        return s;
    }

    static int sellIn(int n) {
        return n;
    }

    static int quality(int n) {
        return n;
    }
}
