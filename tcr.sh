#!/usr/bin/env bash

if mvn clean test ; then
    git add .
    git commit -m "$(echo $@)"
fi
git reset --hard
git status
